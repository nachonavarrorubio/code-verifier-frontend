import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";

//Our CopyRight Component
import { Copyright } from "./CopyRight";

export const StickyFooter = (sx: any) => {
    return (

        <Box sx={{
            display: 'flex',
            flexDirection: "column"
        }}>
            <CssBaseline />
            <Box component="footer"
                sx={{
                    py: 3,
                    px: 2,
                    mt: 'auto',
                    backgroundColor: (theme) => theme.palette.mode === 'light' ? theme.palette.grey[200] : theme.palette.grey[800]
                }}>
                <Container maxWidth="sm">
                    <Copyright sx={{
                        pt: 4
                    }} />
                </Container>

            </Box>
        </Box>
    )

}