import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

export const Copyright = (props: any) => {

    return (
        <Typography variant="body2" color="text-secundary" align="center" {...props}>
{'CopyRight '}
<Link color="inherit" href="https://gitlab.com/nachonavarrorubio">
Nacho's Repo
</Link>
<Link>
{new Date().getFullYear()}
</Link>
        </Typography>


    )
}