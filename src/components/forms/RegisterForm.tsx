import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { AxiosResponse } from "axios";
import { register } from "../../services/authService";

const RegisterForm = () => {


    const initialValues = {
        name: '',
        email: '',
        password: '',
        confirm: '',
        age: 18

    }
    const VALID_PASSWORD_REGEX = /^(?=.*?[A-Z])(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$/;

    //Yup validation Schema
    const registerSchema = Yup.object().shape(
        {
            name: Yup.string()
                .min(6, 'Username must have 6 letters minimun')
                .max(12, 'Username must have maximun 12 letters')
                .required('Username is required'),
            email: Yup.string()
                .email('Invalid email format')
                .required('Email is required'),
            password: Yup.string()
                .min(8, 'Password too short')
                .max(20, 'Password too long')
                .required('Password is required')
                .matches(VALID_PASSWORD_REGEX, 'La contraseña debe tener al menos 8 caracteres, una mayúscula, una minúscula, un número y un carácter especial'),
            confirm: Yup.string()
                .required('You must confirm your password')
                .oneOf([Yup.ref('password')],
                    'Passwords must match')
            ,
            age: Yup.number()
                .min(10, 'You must be over ten years old')
                .required('age is required'),
        }
    )
    return (
        <div>
            <h4>Register as a New User</h4>
            {/* Formik Wrapper */}
            <Formik
                initialValues={initialValues}
                validationSchema={registerSchema}
                onSubmit={async (values) => {
                    
                    register(values.name, values.email, values.password, values.age).then((response: AxiosResponse) => {
                        if (response.status === 200) {
;
                            console.log('User register corretly');
                            console.log(response.data);
                            alert("User created correctly");

                        }
                            else {
                                throw new Error('Error in Registry')
                            }

                       
                    }).catch((error) => {
                        console.error(`[REGISTER ERROR]: something was wrong: ${(error)}`)
                    })
                }}
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>
                            {/*Name field */}
                            <label htmlFor="name">Name</label>
                            <Field id='name' type="text" name="name" placeholder="Your Name" />
                            {/* Name errors */}
                            {
                                errors.name && touched.name && (
                                    <ErrorMessage name="name" component="div"></ErrorMessage>
                                )
                            }
                            {/*Email field */}
                            <label htmlFor="email">Email</label>
                            <Field id='email' type="email" name="email" placeholder="example@gmail.com" />
                            {/* Email errors */}
                            {
                                errors.email && touched.email && (
                                    <ErrorMessage name="email" component="div"></ErrorMessage>
                                )
                            }
                            {/*Passsword field */}
                            <label htmlFor="password">Password</label>
                            <Field id='password' type="password" name="password" placeholder="Password" />
                            {/* Password errors */}
                            {
                                errors.password && touched.password && (
                                    <ErrorMessage name="password" component="div"></ErrorMessage>
                                )
                            }
                            {/*Confirm Passsword field */}
                            <label htmlFor="confirm">Confirm Password</label>
                            <Field id='confirm' type="password" name="confirm" placeholder="Confirm your password" />
                            {/* Confirm Password errors */}
                            {
                                errors.confirm && touched.confirm && (
                                    <ErrorMessage name="confirm" component="div"></ErrorMessage>
                                )
                            }
                            {/*Age field */}
                            <label htmlFor="age">Age</label>
                            <Field id='age' type="number" name="age" placeholder="Age" />
                            {/* Age errors */}
                            {
                                errors.age && touched.age && (
                                    <ErrorMessage name="age" component="div"></ErrorMessage>
                                )
                            }
                            {/*Submit Button */}
                            <button type="submit">Register</button>

                            {/* Message if the form is submitting */}
                            {
                                isSubmitting ? (<p>Sending data to Registry...</p>) : null
                            }
                        </Form>
                    )



                }


            </Formik>


        </div>
    )
}
export default RegisterForm;
