import React, { Fragment, useState } from "react";
import Editor from "react-simple-code-editor";
import { Highlight, Language, themes } from "prism-react-renderer";

const codeSnippet = `

import axios from 'axios';

const getUsert= ()=>{
    return axios.get('https://randomuser.me/api')
}
`
//Define Styles for Editor
const styles: any = {

    root: {
        boxsizing: 'border-box',
        fontFamily: '"Dank Mono", "Fira Code", monospace',
        theme: themes.nightOwl
    }
}

const languages: Language[] = [
    "tsx",
    "typescript",
    "javascript",
    "jsx",
    "python",
    "json",
    "go"
]



//HighLight Component
export const HighLightElement = (code: string) => (
    <Highlight theme={themes.nightOwl} code={code} language={languages[0]}>
        {({ tokens, getLineProps, getTokenProps }) => (
            <Fragment>
                {tokens.map((line, i) => (
                    <div {...getLineProps({ line: line, key: i })}>
                        {line.map((token, key) =>
                            <span {...getTokenProps({ token, key })} />
                        )}
                    </div>)
                )}
            </Fragment>
        )}
    </Highlight>
);

export const NewEditor = () => {

    const [code, setCode] = useState(codeSnippet);
    const [languageSelected, setLanguageSelected] = useState(languages[0]);

    const HandleLanguageChanged = (newValue: any) => {

        setLanguageSelected(newValue);
    }
    const HandleCodeChange = (newCode: string) => {

        setCode(newCode);
    }
    return (
        <div>
            <select >
                {languages.map((language, index) => (
                    <option onChange={(value)=>HandleLanguageChanged(value)} value={language} key={index}>{language}</option>
                ))}
            </select>
            <Editor
                value={code}
                onValueChange={HandleCodeChange}
                highlight={HighLightElement}
                padding={10}
                style={styles.root}
            />
        </div>
    )
}
