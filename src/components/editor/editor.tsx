import react from 'react';

import { Highlight } from 'prism-react-renderer';

interface EditorProps {
    language?: any,
    children?: any,
    solution?:any
    
}

export const Editor = ({ language, children, solution}: EditorProps) => {
// const codeExample=`
// (function somDemo(){
//     var test= "Hello World!";
//     console.log(test);
// })();
// return () => <App/>
// `;

    return (
        <Highlight
        code= {children}
    language= "jsx"  >
            {({ className, style, tokens, getLineProps, getTokenProps }) => (
                <pre className={className} style={style}>
                    {tokens.map((line, i) => (
                        <div {...getLineProps({ line, key: i })}>
                            {line.map((token, key) => (
                                <span {...getTokenProps({ token, key })} />
                            ))}
                        </div>
                    ))}
                </pre>
            )}
        </Highlight>
    );

}
