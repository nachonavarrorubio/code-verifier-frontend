import { Fragment, useState } from "react";
import { Dropzone, ExtFile, FileMosaic, FullScreen, ImagePreview, VideoPreview } from "@dropzone-ui/react";


export const FileUploader = () => {
    const [files, setFiles] = useState<ExtFile[]>([]);
    const [imageSrc, setImageSrc] = useState<any>(undefined);
    const [videoSrc, setVideoSrc] = useState<any>(undefined);

    const updateFiles = (incommingFiles: ExtFile[]) => {
        setFiles(incommingFiles);
    };
    const handleSee = (imageSource: any) => {
        setImageSrc(imageSource);
    };
    const handleWatch = (videoSource: any) => {
        setVideoSrc(videoSource);
    };

    const handleClean = (files: ExtFile) => {
        //console.log("list cleaned", files);
    };

    const removeFile = (id: string | number | undefined) => {
        setFiles(files.filter((x) => x.id !== id));
    };
    return (
        <Fragment>
            <Dropzone
                style={{ minWidth: "505px" }}
                label="Drag'n drop files here or click to browse"
                onClean={handleClean}
                onChange={updateFiles}
                value={files}
                maxFiles={5}
                //maxFileSize={2998000}
                uploadConfig={{
                    url: "http://localhost:8000/api/uploadFile"
                }}
                fakeUpload


            >
                {files.length > 0 && files.map((file: ExtFile) => (
                    <FileMosaic
                        {...file}
                        key={file.id}
                        onDelete={removeFile}
                        onSee={handleSee}
                        onWatch={handleWatch}
                        //info
                        preview
                        resultOnTooltip
                        //localization={"ES-es"}
                    />
                ))}

            </Dropzone>
            <FullScreen
                open={imageSrc !== undefined}
                onClose={(e: any) => setImageSrc(undefined)}
            >
                <ImagePreview
                    src={imageSrc} />
            </FullScreen>
            <FullScreen
                open={videoSrc !== undefined}
                onClose={() => setVideoSrc(undefined)}
            >
                <VideoPreview src={videoSrc} />
            </FullScreen>
        </Fragment>
    );



};