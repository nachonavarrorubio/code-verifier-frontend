import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSessionStorage } from "../hocks/useSessionStorage";
import { getAllKatas } from "../services/katasService";
import { Axios, AxiosResponse } from "axios";
import { date } from "yup";
import { Kata } from "../utils/config/types/Kata.type";


export const KatasPage = () => {
    ;
    let loggedIn = useSessionStorage('sessionToken');
    let navigate = useNavigate();

    const [katas, setKatas] = useState([]); //inital katas is empty
    const [totalPages, setTotalPages] = useState(1) //initial default value
    const [currentPage, setCurrentPage] = useState(1) //initial default value


    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login');
        } else {

            getAllKatas(loggedIn, 2, 1).then((response: AxiosResponse) => {

                if (response.status === 200 && response.data.katas && response.data.totalPages && response.data.currentPage) {
                    console.table(response.data);
                    let { katas, totalPages, currentPage } = response.data;
                    setKatas(katas);
                    setCurrentPage(currentPage);
                    setTotalPages(totalPages);
                } else {
                    throw new Error(`Error obtaining katas: ${response.data}`);
                }

            }).catch((error) => console.error(`[Get All Katas Error] ${error}`));
        }
    }, [loggedIn]);
    /**
     * Method to navigate to Kata Details
     * @param id  of kata to navigate to
     */
    const NavigateToKataDetail = (id: number) => {
        navigate(`/katas/${id}`);
    }
    return (
        <div>
            <h1>
                Katas Page
            </h1>
            {katas.length > 0 ?
                <div>
                        {/* TODO Export to isolated Component */}
                        {katas.map((kata: Kata) => (
                            <div key={kata._id}>
                               <h3 onClick={()=>NavigateToKataDetail(kata._id)}> {kata.name}</h3>
                               <h4>{kata.description}</h4>
                               <h5>Creator: {kata.creator}</h5>
                               <p>Rating: {kata.stars}/5</p>

                            </div>

                        )

                        )}
                </div>

                :
                <div>
                    <h5>No Katas found

                    </h5>
                </div>
            }

        </div>

    )
}