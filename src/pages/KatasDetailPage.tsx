import React, { useState, useEffect } from "react";

//React Router DOM imports
import { useNavigate, useParams } from 'react-router-dom';
import { useSessionStorage } from "../hocks/useSessionStorage";
import { getKataByID } from "../services/katasService";
import { AxiosResponse } from "axios";
import { Kata } from "../utils/config/types/Kata.type";
import { Editor } from "../components/editor/editor";

export const KatasDetailPage = () => {

    let loggedIn = useSessionStorage('sessionToken');
    let navigate = useNavigate();
    //Find Id from Params
    let { id } = useParams();
    const [kata, setKata] = useState<Kata | undefined>(undefined);
    const [ShowSolution, setShowSolution] = useState(false)

    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            if (id) {
                getKataByID(loggedIn, id).then((response: AxiosResponse) => {

                    if (response.status === 200 && response.data) {
                        let kataData: Kata = {
                            _id: response.data._id,
                            name: response.data.name,
                            description: response.data.description,
                            stars: response.data.stars,
                            level: response.data.level,
                            intents: response.data.intents,
                            creator: response.data.creator,
                            solution: response.data.solution,
                            participants: response.data.participants

                        }
                        setKata(kataData);
                        console.table(kataData);
                    }

                }).catch((error) => console.error(`[Kata by ID ERROR]:${error}`))

            } else {
                return navigate('/katas');
            }

        }

    }, [loggedIn]);
    /**
     * Method to navigate to Kata Details
     * @param id  of kata to navigate to
     */
    const NavigateToKataDetail = (id: number) => {
        navigate(`/katas/${id}`);
    }


    return (
        <div>
            <h1>
                Katas Detail Page {id}

            </h1>
            {kata ?
                <div className="kata-data">
                    <h2>{kata?.description}</h2>
                    <h3>Rating: {kata.stars}/5</h3>
                    <button onClick={() => setShowSolution(!ShowSolution)}>
                        {ShowSolution ? 'Show Solution' : 'Hide solution'}
                    </button>
                    {ShowSolution ? null : <Editor>{kata.solution}</Editor>}

                </div>
                :
                <div>
                    <h2>Loading data...</h2>
                </div>
            }

        </div>

    )

}