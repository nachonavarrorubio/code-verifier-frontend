import { AxiosRequestConfig } from "axios";
import axios from "../utils/config/axios.config";
import axiosConfig from "../utils/config/axios.config";
import { number, string } from "yup";

export const getAllKatas = (token: string, limit?: number, page?: number) => {


    //Http://localhost:8000/api/katas?limit=1&page=1
    // Add headers with JWT in x-access.token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            limit: limit,
            page: page

        }

    }

    return axios.get('/katas', options)

};

export const getKataByID = (token: string, id: string) => {
    //Http://localhost:8000/api/katas?limit=1&page=1
    // Add headers with JWT in x-access.token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token
        },
        params: {
            id

        }

    }
    return axios.get('/katas', options)
}