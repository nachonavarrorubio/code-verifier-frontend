import React from 'react';
import './App.css';

import { BrowserRouter as Router } from 'react-router-dom';
import { AppRoutes } from './routes/Routes';
import { Copyright } from './components/Dashboard/CopyRight';
import { StickyFooter } from './components/Dashboard/StickyFooter';


function App() {
  return (
    <div className="App">
      <Router>
        <AppRoutes />
      </Router>
<StickyFooter></StickyFooter>
    </div>
  );
}

export default App;
