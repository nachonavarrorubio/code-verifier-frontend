import axios from "axios";

export default axios.create({
baseURL: 'http://localhost:8000/api', //ruta base que será completada con los endpoints
responseType: 'json',
timeout: 6000
})